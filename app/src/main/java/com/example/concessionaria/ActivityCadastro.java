package com.example.concessionaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ActivityCadastro extends AppCompatActivity {

    //Declaração das variáveis
    Button btAdicionar;
    Button btHoje;
    Spinner spCategoria;
    EditText edValor;
    EditText edData;
    EditText edDescricao;
    BancoDados db = new BancoDados(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        //Inicialização das variáveis
        btAdicionar = (Button)findViewById(R.id.acBtnAdicionar);
        btHoje = (Button)findViewById(R.id.acBtnHoje);
        spCategoria = (Spinner)findViewById(R.id.acSpCategoria);
        edValor = (EditText)findViewById(R.id.acEdtValor);
        edData = (EditText)findViewById(R.id.acEdtData);
        edDescricao = (EditText)findViewById(R.id.acEdtDescricao);


        //Cria as opções da categoria no spinner
        String[] arraySpinner = new String[] {
                "Sedan", "SUV", "Camionete", "Hatch", "Motocicleta"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategoria.setAdapter(adapter);

        //Função do botão hoje
        btHoje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
                Date data = new Date();
                String dataFormatada = formataData.format(data);
                edData.setText(dataFormatada);
            }
        });

        //Função do botão adicionar
        btAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String categoria = spCategoria.getItemAtPosition(spCategoria.getSelectedItemPosition()).toString();
                if (categoria.isEmpty()){
                    Toast.makeText(ActivityCadastro.this, "Categoria obrigatória!", Toast.LENGTH_LONG).show();
                } else {
                    String valor = edValor.getText().toString();
                    if (valor.isEmpty()){
                        Toast.makeText(ActivityCadastro.this, "Valor mínimo obrigatório!", Toast.LENGTH_LONG).show();
                    } else {
                        String descricao = edDescricao.getText().toString();
                        if (descricao.isEmpty()){
                            Toast.makeText(ActivityCadastro.this, "Descrição obrigatória!", Toast.LENGTH_LONG).show();
                        } else {
                            String data = edData.getText().toString();
                            if (data.isEmpty()){
                                Toast.makeText(ActivityCadastro.this, "Data obrigatória!", Toast.LENGTH_LONG).show();
                            } else {
                                //Insert
                                Float valorConvertido = Float.parseFloat(valor);
                                db.addVeiculo(new Veiculo(categoria,descricao,data,valorConvertido));
                                Toast.makeText(ActivityCadastro.this, "Veículo adicionado com sucesso!", Toast.LENGTH_LONG).show();
                                ActivityCadastro.this.finish();
                            }
                        }
                    }
                }
            }
        });



    }
}