package com.example.concessionaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ActivityListagem extends AppCompatActivity {

    Button btVoltar;
    ListView lvVeiculos;
    ArrayList<Veiculo> listaVeiculos = new ArrayList<Veiculo>();
    BancoDados db = new BancoDados(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        btVoltar = (Button)findViewById(R.id.alBtnVoltar);
        lvVeiculos = (ListView)findViewById(R.id.alLvVeiculos);

        listaVeiculos.clear();
        listaVeiculos = db.selectAllVeiculos();

        ListaAdapterVeiculo adapterVeiculo = new ListaAdapterVeiculo(ActivityListagem.this,listaVeiculos);
        lvVeiculos.setAdapter(adapterVeiculo);

        btVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityListagem.this.finish();
            }
        });
    }
}