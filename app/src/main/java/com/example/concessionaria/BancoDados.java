package com.example.concessionaria;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BancoDados extends SQLiteOpenHelper {

    private static final int VERSAO_BANCO = 1;
    private static final String BANCO_VEICULOS = "bd_veiculos";

    private static final String TABELA_VEICULOS = "tb_veiculos";
    private static final String COLUNA_ID = "id";
    private static final String COLUNA_CATEGORIA = "categoria";
    private static final String COLUNA_DESCRICAO = "descricao";
    private static final String COLUNA_DATA = "data";
    private static final String COLUNA_VALOR = "valor";

    public BancoDados(Context context) {
        super(context, BANCO_VEICULOS, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String QUERY = "CREATE TABLE " + TABELA_VEICULOS + "("
                + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUNA_CATEGORIA + " TEXT, "
                + COLUNA_DESCRICAO + " TEXT, "
                + COLUNA_VALOR + " REAL, "
                + COLUNA_DATA + " TEXT)";

        db.execSQL(QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /* CRUD ABAIXO */

    public void addVeiculo(Veiculo veiculo){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUNA_CATEGORIA, veiculo.getCategoria());
        values.put(COLUNA_DESCRICAO, veiculo.getDescricao());
        values.put(COLUNA_DATA, veiculo.getData());
        values.put(COLUNA_VALOR, veiculo.getValor());

        db.insert(TABELA_VEICULOS, null, values);
        db.close();
    }

    public ArrayList<Veiculo> selectAllVeiculos(){
        ArrayList<Veiculo> veiculos = new ArrayList<Veiculo>();

        String query = "SELECT * FROM " + TABELA_VEICULOS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);

        if(c.moveToFirst()){
            do
            {
                Veiculo veiculo = new Veiculo();
                veiculo.setId(Integer.parseInt(c.getString(0)));
                veiculo.setCategoria(c.getString(1));
                veiculo.setDescricao(c.getString(2));
                veiculo.setValor(c.getFloat(3));
                veiculo.setData(c.getString(4));
                veiculos.add(veiculo);
            }while(c.moveToNext());
        }

        return veiculos;
    }
}
