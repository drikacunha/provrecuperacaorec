package com.example.concessionaria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class ListaAdapterVeiculo extends ArrayAdapter<Veiculo> {

    private Context context;
    private ArrayList<Veiculo> lista;

    public ListaAdapterVeiculo(Context context, ArrayList<Veiculo> lista){
        super(context,0,lista);
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Veiculo veiculoPosicao = this.lista.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.veiculo,null);

        TextView tvCategoria = (TextView) convertView.findViewById(R.id.veEdCategoria);
        tvCategoria.setText(veiculoPosicao.getCategoria());

        TextView tvDescricao = (TextView) convertView.findViewById(R.id.veEdDescricao);
        tvDescricao.setText(veiculoPosicao.getDescricao());

        TextView tvData = (TextView) convertView.findViewById(R.id.veEdData);
        tvData.setText(veiculoPosicao.getData());

        TextView tvValor = (TextView) convertView.findViewById(R.id.veEdValor);
        tvValor.setText(Float.toString(veiculoPosicao.getValor()));



        return convertView;

    }

    @Override
    public Veiculo getItem(int position){
        return lista.get(position);
    }

}
