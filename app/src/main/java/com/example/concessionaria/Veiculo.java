package com.example.concessionaria;

import java.util.Date;

public class Veiculo {

    private int id;
    private String categoria;
    private String descricao;
    private String data;
    private Float valor;

    public Veiculo() {

    }

    public Veiculo(String categoria, String descricao, String data, Float valor) {
        this.categoria = categoria;
        this.descricao = descricao;
        this.data = data;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }
}
